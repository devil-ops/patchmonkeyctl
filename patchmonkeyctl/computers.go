package patchmonkeyctl

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/spf13/cobra"
)

// ComputerFilterOpts represents options to filter based on computers
type ComputerFilterOpts struct {
	OsFamily string
	// LinuxOnly   *bool
	// WindowsOnly *bool
	UnSetOnly *bool
	Days      int
}

// DaysDuration returns the days option as a duration
func (o ComputerFilterOpts) DaysDuration() time.Duration {
	return time.Duration(o.Days) * 24 * time.Hour
}

type (
	// ComputerFilter filters computers using a given option
	ComputerFilter func(LastPatchInfoComputersComputer, ComputerFilterOpts) bool
	// ComputerFilterBulk applies all filters sequentionally
	ComputerFilterBulk func([]LastPatchInfoComputersComputer) []string
)

// ComputerFilterOS filters computers by OS Family
func ComputerFilterOS(c LastPatchInfoComputersComputer, o ComputerFilterOpts) bool {
	if o.OsFamily == "" {
		return true
	}
	return strings.EqualFold(fmt.Sprint(c.OsFamily), o.OsFamily)
}

// ComputerFilterUnSet filter to only hosts with an unset patchwindow
func ComputerFilterUnSet(c LastPatchInfoComputersComputer, o ComputerFilterOpts) bool {
	if o.UnSetOnly == nil {
		return true
	}
	if c.LastSuccessfulPatchRun == nil {
		return true
	}
	return false
}

// ComputerFilterDays filters based on the last successful patchrun days-ago
func ComputerFilterDays(c LastPatchInfoComputersComputer, o ComputerFilterOpts) bool {
	if o.Days == 0 {
		return true
	}
	if c.LastSuccessfulPatchRun == nil {
		return true
	}
	if time.Since(*c.LastSuccessfulPatchRun) > o.DaysDuration() {
		return true
	}
	return false
}

// ApplyComputerFilters filters to a set of Computers
func ApplyComputerFilters(records []LastPatchInfoComputersComputer, o ComputerFilterOpts, filters ...ComputerFilter) []LastPatchInfoComputersComputer {
	if len(filters) == 0 {
		return records
	}

	filteredRecords := make([]LastPatchInfoComputersComputer, 0, len(records))

	for _, r := range records {
		keep := true

		for _, f := range filters {
			if !f(r, o) {
				keep = false
				break
			}
		}

		if keep {
			filteredRecords = append(filteredRecords, r)
		}
	}

	return filteredRecords
}

// FilterOptsWithCobra returns a new ComputerFilterOpts from a given cobra.Command
func FilterOptsWithCobra(cmd *cobra.Command) (*ComputerFilterOpts, error) {
	var err error
	o := &ComputerFilterOpts{}
	o.Days, err = cmd.Flags().GetInt("days")
	if err != nil {
		return nil, err
	}
	if cmd.Flags().Changed("linux-only") {
		lo, err := cmd.Flags().GetBool("linux-only")
		if err != nil {
			return nil, err
		}
		if lo {
			o.OsFamily = "LINUX"
		}
	}
	if cmd.Flags().Changed("windows-only") {
		lo, err := cmd.Flags().GetBool("windows-only")
		if err != nil {
			return nil, err
		}
		if lo {
			o.OsFamily = "WINDOWS"
		}
	}

	if cmd.Flags().Changed("unset-only") {
		uo, err := cmd.Flags().GetBool("unset-only")
		if err != nil {
			return nil, err
		}
		o.UnSetOnly = &uo
	}
	return o, nil
}

// BindComputerFilterOptsWithCobra binds flags to a given cobra.Cmd
func BindComputerFilterOptsWithCobra(cmd *cobra.Command) error {
	if cmd == nil {
		return errors.New("cobra.Command must not be nil")
	}
	cmd.PersistentFlags().IntP("days", "d", 21, "Number of days without a successful patch to be considered Out of Date")
	cmd.PersistentFlags().BoolP("linux-only", "l", false, "Only include linux clients")
	cmd.PersistentFlags().BoolP("windows-only", "w", false, "Only include windows clients")
	cmd.PersistentFlags().BoolP("unset-only", "u", false, "Only include hosts that do not have a window set")

	// This doesn't appear to work...
	cmd.MarkFlagsMutuallyExclusive("days", "unset-only")
	return nil
}
