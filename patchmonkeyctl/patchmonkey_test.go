package patchmonkeyctl

import (
	"testing"
	"time"

	"github.com/spf13/cobra"
)

func TestNewOutOfDateReportOptsWithCobra(t *testing.T) {
	cmd := &cobra.Command{}
	if err := BindComputerFilterOptsWithCobra(cmd); err != nil {
		t.Errorf("got unexpected error: %v", err)
	}
	if err := cmd.Execute(); err != nil {
		t.Errorf("got unexpected error: %v", err)
	}
	opts, err := FilterOptsWithCobra(cmd)
	if err != nil {
		t.Fatalf("got unexpected error: %v", err)
	}
	if opts == nil {
		t.Errorf("got unexpected nil result for FilterOptsWithCobra")
	}
}

func TestBindOutOfDateReportOptsCobra(t *testing.T) {
	if err := BindComputerFilterOptsWithCobra(&cobra.Command{}); err != nil {
		t.Errorf("got unexpected error: %v\n", err)
	}
}

func TestBindOutOfDateReportOptsNilCobra(t *testing.T) {
	if err := BindComputerFilterOptsWithCobra(nil); err == nil {
		t.Errorf("expected error but got none")
	}
}

func TestDaysDuration(t *testing.T) {
	o := ComputerFilterOpts{
		Days: 5,
	}

	d := o.DaysDuration()
	if d != time.Duration(5*60*60*24*1000000000) {
		t.Errorf("got unexpected days duration")
	}
}
