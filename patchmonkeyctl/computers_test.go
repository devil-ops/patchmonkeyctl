package patchmonkeyctl

import (
	"testing"
	"time"
)

func TestComputerFilterUnSet(t *testing.T) {
	// truthy := true
	// falsy := false
	if got := ComputerFilterUnSet(LastPatchInfoComputersComputer{}, ComputerFilterOpts{
		UnSetOnly: toPTR(true),
	}); got != true {
		t.Errorf("expected to get true")
	}

	if got := ComputerFilterUnSet(LastPatchInfoComputersComputer{
		LastSuccessfulPatchRun: toPTR(time.Now()),
	}, ComputerFilterOpts{
		UnSetOnly: toPTR(false),
	}); got != false {
		t.Errorf("expected to get false")
	}
}

func TestComputerFilterOS(t *testing.T) {
	// Match Empty
	if got := ComputerFilterOS(LastPatchInfoComputersComputer{
		OsFamily: "LINUX",
	}, ComputerFilterOpts{}); got != true {
		t.Errorf("expected true")
	}

	// Match Linux
	if got := ComputerFilterOS(LastPatchInfoComputersComputer{
		OsFamily: "LINUX",
	}, ComputerFilterOpts{
		OsFamily: "LINUX",
	}); got != true {
		t.Errorf("expected true")
	}

	// Do NOT Match Linux
	if got := ComputerFilterOS(LastPatchInfoComputersComputer{
		OsFamily: "LINUX",
	}, ComputerFilterOpts{
		OsFamily: "WINDOWS",
	}); got != false {
		t.Errorf("expected false")
	}
}

func TestApplyComputerFilters(t *testing.T) {
	got := ApplyComputerFilters([]LastPatchInfoComputersComputer{
		{OsFamily: "WINDOWS"},
		{OsFamily: "LINUX"},
	}, ComputerFilterOpts{
		OsFamily: "Linux",
	}, ComputerFilterOS)

	if len(got) != 1 {
		t.Fatalf("expected 1 result but got: %v", len(got))
	}
	if got[0].OsFamily != "LINUX" {
		t.Errorf("expected to find OsFamily LINUX but got: %v", got[0].OsFamily)
	}
}

func toPTR[V any](v V) *V {
	return &v
}
