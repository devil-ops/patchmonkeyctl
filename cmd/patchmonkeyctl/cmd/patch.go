package cmd

import (
	"errors"
	"fmt"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
)

// patchCmd represents the patch command
var patchCmd = &cobra.Command{
	Use:   "patch <computer> [<computer>...]",
	Args:  cobra.MinimumNArgs(1),
	Short: "Patch a computer (or computers) in Patchmonkey",
	Example: `# Patch a single computer
patchmonkeyctl patch foo.oit.duke.edu

# Patch multiple computers
patchmonkeyctl patch foo.oit.duke.edu bar.oit.duke.edu

# Patch a machine and skip confirmation prompt
patchmonkeyctl patch foo.oit.duke.edu --skip-confirm`,

	RunE: func(cmd *cobra.Command, args []string) error {
		skipConfirm := mustGetCmd[bool](*cmd, "skip-confirm")
		all := make([]patchmonkey.PatchComputerPatchComputerNowPatchComputerNowPayload, len(args))
		for idx, c := range args {
			if !skipConfirm {
				if !askForConfirmation(fmt.Sprintf("🥳  About to patch computer: %v", c), "This may reboot the target computer. Continue?") {
					return errors.New("quitting due to no confirmation")
				}
			}
			r, err := patchmonkey.PatchComputer(ctx, client, c)
			if err != nil {
				return err
			}
			all[idx] = r.PatchComputerNow
		}
		gout.MustPrint(all)
		return nil
	},
}

func init() {
	patchCmd.PersistentFlags().Bool("skip-confirm", false, "Skip confirmation prompt when starting a patching run")
	rootCmd.AddCommand(patchCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// patchCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// patchCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
