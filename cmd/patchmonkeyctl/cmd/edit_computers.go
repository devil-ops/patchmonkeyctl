package cmd

import (
	"errors"
	"fmt"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/patchmonkeyctl/patchmonkeyctl"
)

// editComputersCmd represents the editComputers command
var editComputersCmd = &cobra.Command{
	Use:     "computers",
	Aliases: []string{"computer", "c", "cs"},
	Short:   "edit information about computers",
	Args:    cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		var v []any
		skipConfirm := mustGetCmd[bool](*cmd, "skip-confirm")
		if cmd.Flags().Changed("window") {
			window := mustGetCmd[int](*cmd, "window")
			if !skipConfirm && !askForConfirmation(
				"Assigning computers to patch window",
				fmt.Sprintf("Computers: %v\nWindow: %v\nContinue?", args, window),
			) {
				return errors.New("quitting due to no confirmation")
			}
			for _, host := range args {
				resp, err := patchmonkeyctl.SetPatchWindows(ctx, client, host, fmt.Sprint(window))
				if err != nil {
					return err
				}
				v = append(v, resp.SetComputerPatchWindow.Computer)
			}
			// gout.MustPrint(ret)
		}

		if cmd.Flags().Changed("strategy") {
			strategy := mustGetCmd[string](*cmd, "strategy")
			if !skipConfirm && !askForConfirmation(
				"Assigning computers to patch strategy",
				fmt.Sprintf("Computers: %v\nStrategy: %v\nContinue?", args, strategy),
			) {
				return errors.New("quitting due to no confirmation")
			}
			for _, host := range args {
				resp, err := patchmonkeyctl.SetPatchStrategy(ctx, client, host, strategy)
				if err != nil {
					return err
				}
				v = append(v, resp.UpdateComputer.Computer)
			}
		}
		return gout.Print(v)
	},
}

func init() {
	editCmd.AddCommand(editComputersCmd)
	editComputersCmd.Flags().IntP("window", "w", 0, `The patch window id to assign the computer to. Use "-1" for "Any - outside of business hours"`)
	editComputersCmd.Flags().StringP("strategy", "s", "", `The patch strategy to use for patching`)
	editComputersCmd.MarkFlagsOneRequired("window", "strategy")
	editComputersCmd.PersistentFlags().Bool("skip-confirm", false, "Skip confirmation prompt when editing an object")
}
