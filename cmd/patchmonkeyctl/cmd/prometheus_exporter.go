package cmd

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"sort"
	"time"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/patchmonkeyctl/patchmonkeyctl"
)

var promExportCmd = &cobra.Command{
	Use:   "prometheus-exporter",
	Short: "Expose patchmonkey metrics to prometheus",
	RunE: func(cmd *cobra.Command, _ []string) error {
		endpoint := mustGetCmd[string](*cmd, "endpoint")
		addr := mustGetCmd[string](*cmd, "address")
		pinterval := mustGetCmd[time.Duration](*cmd, "poll-interval")

		h := newPromHandler(
			withInterval(pinterval),
		)
		go h.pollPatchmonkey()

		http.Handle(endpoint, h)
		http.Handle("/healthz", h.healthCheckHandler())

		server := &http.Server{
			Addr:              addr,
			ReadHeaderTimeout: 30 * time.Second,
		}

		slog.Info("starting listener", "address", addr, "endpoint", endpoint, "poll-interval", pinterval)

		return server.ListenAndServe()
	},
}

func init() {
	rootCmd.AddCommand(promExportCmd)
	promExportCmd.PersistentFlags().StringP("address", "a", ":9633", "address for prometheus exporter to listen on")
	promExportCmd.PersistentFlags().StringP("endpoint", "e", "/metrics", "endpoint url for metrics to be served on")
	promExportCmd.PersistentFlags().DurationP("poll-interval", "p", time.Minute*5, "time to wait between patchmonkey polls")
}

func (p *promHandler) healthCheckHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		if p.handlerError != nil {
			w.WriteHeader(http.StatusInternalServerError)
			if _, err := w.Write([]byte("error polling metrics: " + p.handlerError.Error())); err != nil {
				slog.Warn("could not write health check response", "error", err)
			}
			return
		}
		w.WriteHeader(http.StatusOK)
		if _, err := w.Write([]byte("ok")); err != nil {
			slog.Warn("could not write health check response", "error", err)
		}
	})
}

type promHandler struct {
	summaries    map[string]*computerRunSummary
	interval     time.Duration
	page         []byte
	handlerError error
}

func newPromHandler(opts ...func(*promHandler)) *promHandler {
	p := &promHandler{
		summaries:    map[string]*computerRunSummary{},
		interval:     time.Minute * 5,
		handlerError: errors.New(`polling not yet completed`), // default to error state
	}
	for _, opt := range opts {
		opt(p)
	}
	return p
}

func (p promHandler) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
	warnWrite(w, p.page)
}

func warnWrite(w http.ResponseWriter, b []byte) {
	if _, err := w.Write(b); err != nil {
		slog.Warn("could not write prometheus results", "error", err)
	}
}

func (p *promHandler) pollPatchmonkey() {
	for {
		started := time.Now()
		cl, err := patchmonkeyctl.PatchRuns(context.Background(), client)
		if err != nil {
			slog.Warn("error polling patchmonkey", "error", err)
			time.Sleep(p.interval)
			p.handlerError = err
			continue
		}
		if len(cl.Computers) == 0 {
			slog.Warn("no computers found when polling patchmonkey")
			time.Sleep(p.interval)
			p.handlerError = errors.New(`no results are coming back from patchmonkey.oit.duke.edu. 
			If this issue persists check with the upstream patchmonkey application`)
			continue
		}
		for _, comp := range cl.Computers {
			s := computerRunSummary{
				os:       string(comp.OsFamily),
				statuses: map[string]int{},
			}
			if !comp.SkipPatchingUntil.IsZero() {
				s.skipUntil = time.Since(comp.SkipPatchingUntil).Hours() / -24
			}

			for _, run := range comp.Runs {
				if _, ok := s.statuses[string(run.Status)]; !ok {
					s.statuses[string(run.Status)] = 0
				}
				s.statuses[string(run.Status)]++
			}
			if len(s.statuses) != 0 {
				p.summaries[comp.Name] = &s
			}
		}
		p.page = prepareMetrics(p.summaries)
		slog.Debug("completed poll", "took", time.Since(started))
		p.handlerError = nil
		time.Sleep(p.interval)
	}
}

func prepareMetrics(m map[string]*computerRunSummary) []byte {
	// patch_run_status first
	b := bytes.NewBufferString(`# HELP patch_run_status Status of patch runs from patchmonkey
# TYPE patch_run_status counter
`)
	metrics := []string{}
	for client, summary := range m {
		for status, count := range summary.statuses {
			metrics = append(metrics, fmt.Sprintf("patch_run_status{client=\"%v\",os=\"%v\",status=\"%v\"} %v\n", client, summary.os, status, count))
		}
	}
	sort.Strings(metrics)
	for _, m := range metrics {
		b.Write([]byte(m))
	}

	// Now do patch_run_skip_until
	b.WriteString(`# HELP patch_run_skip_until_days Total number of seconds until a host will patch
# TYPE patch_run_skip_until_days gauge
`)
	metrics = []string{}
	for client, summary := range m {
		if summary.skipUntil > 0 {
			metrics = append(metrics, fmt.Sprintf("patch_run_skip_until_days{client=\"%v\",os=\"%v\"} %v\n", client, summary.os, summary.skipUntil))
		}
	}
	sort.Strings(metrics)
	for _, m := range metrics {
		b.Write([]byte(m))
	}
	return b.Bytes()
}

func withInterval(d time.Duration) func(*promHandler) {
	return func(p *promHandler) {
		p.interval = d
	}
}

type computerRunSummary struct {
	os        string
	statuses  map[string]int
	skipUntil float64
}
