package cmd

import (
	"fmt"
	"log/slog"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
	"gitlab.oit.duke.edu/devil-ops/patchmonkeyctl/patchmonkeyctl"
)

// assignCmd represents the assign command
var assignCmd = &cobra.Command{
	Use:   "assign COMPUTER [COMPUTER...] [-w WINDOW_ID]",
	Short: "Assign a computer to a patch window",
	Long: `Assign a computer to a patch window. If you don't know the id of the window you
are attempting to assign to, you can see them all with the 'get patchwindows'
command.`,
	Args:       cobra.MinimumNArgs(1),
	Deprecated: "use 'patchmonkeyctl edit computer COMPUTER -w ID' instead",
	RunE: func(cmd *cobra.Command, args []string) error {
		// Get the window id
		window := mustGetCmd[int](*cmd, "window")
		// Figure out what the current assignments are
		ids := make([]string, len(args))
		for idx, c := range args {
			id, err := patchmonkey.GetComputerIDWithName(ctx, client, c)
			if err != nil {
				return err
			}
			ids[idx] = id
		}
		updates := make([]patchmonkeyctl.SetPatchWindowWithIdsSetComputerPatchWindowSetComputerPatchWindowPayloadComputer, len(ids))
		for idx, id := range ids {
			update, err := patchmonkeyctl.SetPatchWindowWithIds(ctx, client, id, fmt.Sprint(window))
			if err != nil {
				return err
			}
			updates[idx] = update.SetComputerPatchWindow.Computer
		}
		slog.Info("assigned computers to patch window", "computer-ids", ids, "id", fmt.Sprint(window))
		gout.MustPrint(updates)
		return nil
	},
}

func init() {
	rootCmd.AddCommand(assignCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// assignCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// assignCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	assignCmd.Flags().IntP("window", "w", -1, `The patch window id to assign the computer to. Defaults to -1 which is "Any - outside of business hours"`)
}
