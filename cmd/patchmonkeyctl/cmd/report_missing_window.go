package cmd

import (
	"errors"
	"fmt"
	"log/slog"
	"os"
	"regexp"
	"sort"
	"strings"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/patchmonkeyctl/patchmonkeyctl"
	"golang.org/x/term"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
	"gopkg.in/yaml.v2"
)

// reportMissingWindowCmd represents the reportOutOfDate command
var reportMissingWindowCmd = &cobra.Command{
	Use:     "missing-window",
	Aliases: []string{"mw"},
	Short:   "Report computers that are not part of a patch window and don't have a reason for it",
	RunE: func(cmd *cobra.Command, _ []string) error {
		config := mustGetCmd[string](*cmd, "config")
		noConfig := mustGetCmd[bool](*cmd, "no-config")

		var mc missingConfig
		switch {
		case config == "" && !noConfig:
			return errors.New("no config given. Please pass either the --config=FILE paramater in, or use --no-config for ALL hosts")
		case noConfig:
			mc = missingConfig{}
		case config != "":
			b, cerr := os.ReadFile(config) // nolint:gosec
			cobra.CheckErr(cerr)
			cobra.CheckErr(yaml.Unmarshal(b, &mc))
		default:
			panic("how did we get here??")
		}

		excludeR := make([]regexp.Regexp, len(mc.Excludes))
		for idx, item := range mc.Excludes {
			r, rerr := regexp.Compile(item)
			if rerr != nil {
				return rerr
			}
			excludeR[idx] = *r
		}

		// Figure out what options to pass to the filter
		// o, err := patchmonkey.Computers(ctx, *client)
		o, err := patchmonkeyctl.ComputerPatchWindows(ctx, client)
		if err != nil {
			return err
		}
		report := map[string][]string{}
		caser := cases.Title(language.English)
		for _, item := range o.Computers {
			keep := true
			for _, reg := range excludeR {
				if reg.Match([]byte(item.Name)) {
					keep = false
					break
				}
			}
			if !keep {
				continue
			}
			if (item.PatchWindow == patchmonkeyctl.ComputerPatchWindowsComputersComputerPatchWindow{}) && (item.ExceptionReason == "") {
				switch {
				case (len(mc.RequiredStrings) > 0) && !containsString(item.Name, mc.RequiredStrings):
					slog.Info("ignoring host because it does not contain a required string", "host", item.Name)
				case containsString(item.Name, mc.ForbiddenStrings):
					slog.Info("ignoring host because it contains a forbidden string", "host", item.Name)
				default:
					osfam := fmt.Sprint(item.OsFamily)
					existing := report[osfam]
					report[osfam] = append(existing, item.Name)
				}
			}
		}
		if len(report) == 0 {
			slog.Info("Wow, no hosts are missing patch windows! 🎉")
			return nil
		}
		width, _, _ := term.GetSize(0)
		hostname, _ := os.Hostname()
		configLocation := config
		if hostname != "" {
			configLocation = fmt.Sprintf("%v:%v", hostname, config)
		}
		for _, os := range sortKeys(report) {
			fmt.Println(strings.Repeat("✂️", width))
			sort.Strings(report[os])
			fmt.Printf("Hosts missing patch windows on %v:\n", caser.String(os))
			gout.MustPrint(report[os])
			if config != "" {
				fmt.Printf(`
If any of the above hosts should be excluded, add them to the config file:

%v

This will prevent them from being in future reports.

Thanks for keeping Duke safe by ensuring everything is patched! ❤️

`, configLocation)
			}
			fmt.Println(strings.Repeat("✂️", width))
		}
		return nil
	},
}

func containsString(s string, ss []string) bool {
	for _, item := range ss {
		if strings.Contains(s, item) {
			return true
		}
	}
	return false
}

func sortKeys(m map[string][]string) []string {
	items := []string{}
	for item := range m {
		items = append(items, item)
	}
	sort.Strings(items)
	return items
}

func init() {
	reportCmd.AddCommand(reportMissingWindowCmd)
	reportMissingWindowCmd.PersistentFlags().String("config", "", "Use the given file for configuration params like excludes")
	reportMissingWindowCmd.PersistentFlags().Bool("no-config", false, "Pass this if you really want to run with no config file")
	reportMissingWindowCmd.MarkFlagsMutuallyExclusive("config", "no-config")
}

type missingConfig struct {
	Excludes         []string `yaml:"exclude"`
	RequiredStrings  []string `yaml:"required_strings"`
	ForbiddenStrings []string `yaml:"forbidden_strings"`
}
