package cmd

import (
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
)

// testCmd represents the test command
var testCmd = &cobra.Command{
	Use:   "test",
	Args:  cobra.MinimumNArgs(1),
	Short: "Perform a connection test on a set of computers",
	RunE: func(_ *cobra.Command, args []string) error {
		var all []patchmonkey.ConnectionTestTestConnectionTestConnectionPayload
		for _, c := range args {
			r, err := patchmonkey.ConnectionTest(ctx, client, c)
			if err != nil {
				return err
			}
			all = append(all, r.TestConnection)
		}
		gout.MustPrint(all)
		return nil
	},
}

func init() {
	rootCmd.AddCommand(testCmd)
}
