package cmd

import (
	"errors"
	"testing"
)

func TestPanicIfErr(t *testing.T) {
	// Test case 1: msg is nil, no panic should occur
	// Arrange
	err := error(nil)

	// Act
	// Assert - no panic should occur
	defer func() {
		if r := recover(); r != nil {
			t.Errorf("Expected no panic, but got: %v", r)
		}
	}()
	panicIfErr(err)

	// Test case 2: err is not nil, panic should occur
	// Arrange
	err = errors.New("test error")

	// Act
	// Assert - panic should occur
	defer func() {
		if r := recover(); r == nil {
			t.Error("Expected panic, but no panic occurred")
		}
	}()
	panicIfErr(err)
}
