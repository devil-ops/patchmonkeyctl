package cmd

import (
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
)

// getMyInfoCmd represents the getMyInfo command
var getMyInfoCmd = &cobra.Command{
	Use:     "myinfo",
	Short:   "Get information about the current computer (Unauthenticated)",
	Aliases: []string{"my-info"},
	Args:    cobra.ExactArgs(0),
	RunE: func(_ *cobra.Command, _ []string) error {
		mi, err := patchmonkey.GetMyInfo("")
		if err != nil {
			return err
		}
		gout.MustPrint(mi)
		return nil
	},
}

func init() {
	getCmd.AddCommand(getMyInfoCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getMyInfoCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getMyInfoCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
