package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
	"gitlab.oit.duke.edu/devil-ops/patchmonkeyctl/patchmonkeyctl"
)

var getRunsCmd = &cobra.Command{
	Use:   "runs [COMPUTER...]",
	Short: "Get information about a computers patch runs",
	RunE: func(_ *cobra.Command, args []string) error {
		if len(args) == 0 {
			c, err := patchmonkeyctl.PatchRuns(context.Background(), client)
			if err != nil {
				return err
			}
			gout.MustPrint(c.Computers)
		} else {
			ids := make([]string, len(args))
			for idx, computer := range args {
				id, err := patchmonkey.GetComputerIDWithName(ctx, client, computer)
				if err != nil {
					return err
				}
				ids[idx] = id
			}
			c, err := patchmonkeyctl.PatchRunsWithComputerIDs(context.Background(), client, ids)
			if err != nil {
				return err
			}
			gout.MustPrint(c.Computers)
		}
		return nil
	},
}

func init() {
	getCmd.AddCommand(getRunsCmd)
}
