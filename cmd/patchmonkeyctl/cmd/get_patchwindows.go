package cmd

import (
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
	"gitlab.oit.duke.edu/devil-ops/patchmonkeyctl/patchmonkeyctl"
)

// getPatchWindowsCmd represents the getPatchWindows command
var getPatchWindowsCmd = &cobra.Command{
	Use:     "patchwindows",
	Aliases: []string{"patchwindow", "pw", "pws"},
	Short:   "Get information about the patch windows",
	RunE: func(cmd *cobra.Command, _ []string) error {
		var i interface{}
		if mustGetCmd[bool](*cmd, "detailed") {
			raw, err := patchmonkey.PatchWindows(ctx, client)
			if err != nil {
				return err
			}
			i = raw.PatchWindows
		} else {
			raw, err := patchmonkeyctl.PatchWindowsBasic(ctx, client)
			if err != nil {
				return err
			}
			i = raw.PatchWindows
		}
		gout.MustPrint(i)
		return nil
	},
}

func init() {
	getCmd.AddCommand(getPatchWindowsCmd)
	getPatchWindowsCmd.Flags().BoolP("detailed", "d", false, "Show full details on patch windows, instead of just the id and pretty name")
}
