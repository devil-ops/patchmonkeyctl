package cmd

import (
	"fmt"
	"log/slog"
	"time"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/patchmonkeyctl/patchmonkeyctl"
)

var reportFrequentFailsCmd = &cobra.Command{
	Use:     "frequent-fails",
	Aliases: []string{"ff"},
	Short:   "Report computers that are failing frequently",
	RunE: func(cmd *cobra.Command, _ []string) error {
		linuxOnly := mustGetCmd[bool](*cmd, "linux-only")
		windowsOnly := mustGetCmd[bool](*cmd, "windows-only")
		lookbackI := mustGetCmd[int](*cmd, "lookback-weeks")
		minFailures := mustGetCmd[int](*cmd, "min-num-failures")
		minPercentFailures := mustGetCmd[float64](*cmd, "min-percent-failures")
		days := 7 * lookbackI
		earliest := time.Now().AddDate(0, 0, -days)

		slog.Info("looking back", "days", days, "earliest", earliest)
		// Get an inventory of all the hosts
		// This is a slow process right now. Once we have some filters for osFamily and run.StartTime, we should be able to improve this speed
		i, err := patchmonkeyctl.PatchRuns(ctx, client)
		if err != nil {
			return err
		}

		ret := []failingComputer{}

		for _, item := range i.Computers {
			// Ignore things that have never had a run
			if len(item.Runs) == 0 {
				continue
			}
			if linuxOnly && (item.OsFamily != "LINUX") {
				continue
			}
			if windowsOnly && (item.OsFamily != "WINDOWS") {
				continue
			}
			errCount := 0
			successCount := 0
			for _, run := range item.Runs {
				if !run.StartTime.After(earliest) {
					continue
				}
				if run.Status == "ERROR" {
					errCount++
				} else {
					successCount++
				}
			}
			if errCount == 0 {
				continue
			}
			errPer := (float64(errCount) / float64((errCount + successCount)) * 100)

			if errPer < minPercentFailures {
				continue
			}
			if errCount < minFailures {
				continue
			}
			ret = append(ret, failingComputer{
				Name:        item.Name,
				Fails:       errCount,
				Successes:   successCount,
				FailPercent: fmt.Sprintf("%.2f%%", errPer),
			})

		}
		gout.MustPrint(ret)
		return nil
	},
}

type failingComputer struct {
	Name        string
	Fails       int
	Successes   int
	FailPercent string
}

func init() {
	reportCmd.AddCommand(reportFrequentFailsCmd)
	reportCmd.PersistentFlags().BoolP("linux-only", "l", false, "only include linux clients")
	reportCmd.PersistentFlags().BoolP("windows-only", "w", false, "only include windows clients")
	reportCmd.PersistentFlags().IntP("lookback-weeks", "k", 5, "how many weeks ago to look back")
	reportCmd.PersistentFlags().IntP("min-num-failures", "n", 2, "minimum number of failures to include in the report")
	reportCmd.PersistentFlags().Float64P("min-percent-failures", "p", 100, "minimum percentage of failures to include in the report")
	// reportCmd.MarkFlagsMutuallyExclusive("min-num-failures", "min-percent-failures")
	// cobra.CheckErr(patchmonkeyctl.BindComputerFilterOptsWithCobra(reportFrequentFailsCmd))
}
