package cmd

import (
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
)

// getComputersCmd represents the getComputers command
var getComputersCmd = &cobra.Command{
	Use:     "computers",
	Aliases: []string{"computer", "c", "cs"},
	Short:   "Get information about computers",
	RunE: func(_ *cobra.Command, args []string) error {
		if len(args) == 0 {
			o, err := patchmonkey.Computers(ctx, client)
			if err != nil {
				return err
			}
			gout.MustPrint(o.Computers)
		} else {
			var ids []string
			for _, computer := range args {

				id, err := patchmonkey.GetComputerIDWithName(ctx, client, computer)
				if err != nil {
					return err
				}
				ids = append(ids, id)
			}
			c, err := patchmonkey.ComputersWithId(ctx, client, ids)
			if err != nil {
				return err
			}
			gout.MustPrint(c.Computers)
		}
		return nil
	},
}

func init() {
	getCmd.AddCommand(getComputersCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getComputersCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getComputersCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
