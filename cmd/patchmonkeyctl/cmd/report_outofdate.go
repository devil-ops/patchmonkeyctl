package cmd

import (
	"log/slog"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/patchmonkeyctl/patchmonkeyctl"
)

// reportOutOfDateCmd represents the reportOutOfDate command
var reportOutOfDateCmd = &cobra.Command{
	Use:     "out-of-date",
	Aliases: []string{"ood"},
	Short:   "Report computers that have not patched within a given time period",
	RunE: func(cmd *cobra.Command, _ []string) error {
		// Figure out what options to pass to the filter
		opts, err := patchmonkeyctl.FilterOptsWithCobra(cmd)
		if err != nil {
			return err
		}
		slog.Debug("filter options", "opts", opts)

		// Get an inventory of all the hosts
		i, err := patchmonkeyctl.LastPatchInfo(ctx, client)
		if err != nil {
			return err
		}

		// Filter down to only what we want
		computers := patchmonkeyctl.ApplyComputerFilters(
			i.Computers,
			*opts,
			patchmonkeyctl.ComputerFilterOS,
			patchmonkeyctl.ComputerFilterUnSet,
			patchmonkeyctl.ComputerFilterDays,
		)
		gout.MustPrint(computers)
		return nil
	},
}

func init() {
	reportCmd.AddCommand(reportOutOfDateCmd)
	cobra.CheckErr(patchmonkeyctl.BindComputerFilterOptsWithCobra(reportOutOfDateCmd))
}
