package cmd

import (
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
)

// getMeCmd represents the getMe command
var getMeCmd = &cobra.Command{
	Use:   "me",
	Short: "Get information about the current user",
	RunE: func(_ *cobra.Command, _ []string) error {
		i, err := patchmonkey.Me(ctx, client)
		if err != nil {
			return err
		}
		gout.MustPrint(i.CurrentUser)
		return nil
	},
}

func init() {
	getCmd.AddCommand(getMeCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getMeCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getMeCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
