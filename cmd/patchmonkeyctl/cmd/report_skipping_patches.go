package cmd

import (
	"time"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/patchmonkeyctl/patchmonkeyctl"
)

var reportSkipUntilCmd = &cobra.Command{
	Use:     "skipped-patches",
	Aliases: []string{"sp", "skip-patches", "skipped", "skipping"},
	Short:   "Report computers that are set to skip patches",
	RunE: func(_ *cobra.Command, _ []string) error {
		ret := []patchmonkeyctl.SkipUntilComputersComputer{}
		resp, err := patchmonkeyctl.SkipUntil(ctx, client)
		if err != nil {
			return err
		}
		for _, item := range resp.Computers {
			if item.SkipPatchingUntil.IsZero() || time.Since(item.SkipPatchingUntil) > 0 {
				continue
			}
			ret = append(ret, item)
		}
		return gout.Print(ret)
	},
}

func init() {
	reportCmd.AddCommand(reportSkipUntilCmd)
}
