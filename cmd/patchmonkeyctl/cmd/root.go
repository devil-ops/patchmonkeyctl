/*
Package cmd is the CLI package for Patchmonkey
*/
package cmd

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/charmbracelet/log"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"

	goutbra "github.com/drewstinnett/gout-cobra"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var (
	cfgFile    string
	verbose    bool
	timestamps bool
	client     *patchmonkey.Client
	ctx        context.Context
	// writer     io.Writer
	version = "dev"
	baseURL = "https://patchmonkey.oit.duke.edu"
	logger  *slog.Logger
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:           "patchmonkeyctl",
	Short:         "Interact with PatchMonkey 🐒 though the command line!",
	Version:       version,
	SilenceUsage:  true,
	SilenceErrors: true,
	PersistentPreRunE: func(cmd *cobra.Command, _ []string) error {
		if err := goutbra.Cmd(cmd); err != nil {
			return err
		}

		ctx = context.Background()
		// Setup client
		if cmd.Use != "version" && cmd.Use != "docgen" && cmd.Use != "help" && cmd.Use != "myinfo" {
			token := viper.GetString("token")
			if token == "" {
				return errors.New("no token provided. Please set it in with PATCHMONKEY_TOKEN or pass it with --token/-t")
			}
			client = patchmonkey.New(patchmonkey.WithLLT(token))
		}
		return nil
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		slog.Warn("fatal error", "error", err)
		os.Exit(2)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.patchmonkeyctl.yaml)")
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Verbose output")
	rootCmd.PersistentFlags().BoolVar(&timestamps, "timestamps", false, "Include timestamps in output logs")
	rootCmd.PersistentFlags().StringVar(&baseURL, "base-url", "https://patchmonkey.oit.duke.edu", "Change the base URL for connectionsj")
	rootCmd.PersistentFlags().StringP("token", "t", "", "API token. For information on how to get one, see https://duke.is/rgpp4")
	if err := viper.BindPFlag("token", rootCmd.PersistentFlags().Lookup("token")); err != nil {
		panic(err)
	}
	if err := goutbra.Bind(rootCmd); err != nil {
		panic(err)
	}
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	initLogging()
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".cli" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".patchmonkeyctl")
	}

	viper.SetEnvPrefix("patchmonkey")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}

func initLogging() {
	// cobra.OnInitialize(initConfig)
	lopts := log.Options{
		TimeFormat: time.StampMilli,
		Prefix:     "patchmonkeyctl 🐵 ",
	}
	if verbose {
		lopts.Level = log.DebugLevel
	}
	if timestamps {
		lopts.ReportTimestamp = true
	}
	logger = slog.New(log.NewWithOptions(
		os.Stderr,
		lopts,
	))
	slog.SetDefault(logger)
}
