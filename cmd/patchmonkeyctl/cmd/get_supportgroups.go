package cmd

import (
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-patchmonkey/patchmonkey"
)

// getSupportGroupsCmd represents the getSupportGroups command
var getSupportGroupsCmd = &cobra.Command{
	Use:     "supportgroups",
	Aliases: []string{"supportgroup", "sg", "sgs"},
	Short:   "Get information about support groups",
	RunE: func(_ *cobra.Command, _ []string) error {
		i, err := patchmonkey.SupportGroups(ctx, client)
		if err != nil {
			return err
		}
		gout.MustPrint(i)
		return nil
	},
}

func init() {
	getCmd.AddCommand(getSupportGroupsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getSupportGroupsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getSupportGroupsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
