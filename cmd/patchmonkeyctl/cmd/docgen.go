package cmd

import (
	"fmt"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
)

// docgenCmd represents the docgen command
var docgenCmd = &cobra.Command{
	Use:    "docgen",
	Short:  "Generate documentation",
	Hidden: true,
	RunE: func(_ *cobra.Command, _ []string) error {
		const fmTemplate = `---
date: %s
title: "%s"
slug: %s
url: %s
---
`

		filePrepender := func(filename string) string {
			now := time.Now().Format(time.RFC3339)
			name := filepath.Base(filename)
			base := strings.TrimSuffix(name, path.Ext(name))
			url := "/commands/" + strings.ToLower(base) + "/"
			return fmt.Sprintf(fmTemplate, now, strings.ReplaceAll(base, "_", " "), base, url)
		}
		return doc.GenMarkdownTreeCustom(rootCmd, "./docs/content/", filePrepender, linkHandler)
	},
}

func linkHandler(name string) string {
	return name
}

func init() {
	rootCmd.AddCommand(docgenCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// docgenCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// docgenCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
