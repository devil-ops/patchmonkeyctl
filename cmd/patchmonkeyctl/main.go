/*
Package main is the executable module
*/
package main

import "gitlab.oit.duke.edu/devil-ops/patchmonkeyctl/cmd/patchmonkeyctl/cmd"

func main() {
	cmd.Execute()
}
