# Patchmonkey CTL

Interact with Patchmonkey over the CLI.

## Installation

Download a package from the
[releases](https://gitlab.oit.duke.edu/devil-ops/patchmonkeyctl/-/releases)
page, or use the [devil-ops
package](https://gitlab.oit.duke.edu/devil-ops/installing-devil-ops-packages)
for homebrew, yum, etc.

## Usage

See `patchmonkeyctl -h` for full usage information.

### Authentication

Authentication is done using the OIDC Token service from IDM. Go
[here](https://duke.is/rgpp4) for information on getting a token to use.

Once you have the token, set it as `PATCHMONKEY_TOKEN` in your environment.

### Commands

#### Get information

Use `patchmonkeyctl get` to get information about support groups, patch windows
or computers.

#### Test Connection

Test the connection with:

```bash
$ patchmonkeyctl test $COMPUTER [$COMPUTER...]
...
```

#### Patch Now

Patch a computer or computers now

```bash
$ patchmonkeyctl patch $COMPUTER [$COMPUTER...]
...
```
